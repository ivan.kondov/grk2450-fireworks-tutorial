from fireworks import Firework
from fireworks import Workflow
from fireworks import ScriptTask
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire

# construct the workflow
fw1 = Firework(ScriptTask.from_str('echo "To be, or not to be,"'),
               name='First act')
fw2 = Firework(ScriptTask.from_str('echo "that is the question:"'),
               name='Second act')
workflow = Workflow([fw1, fw2], links_dict={fw1: fw2}, name='Hamlet workflow')

# add the workflow to launchpad
launchpad = LaunchPad.from_file('launchpad.yaml')
launchpad.add_wf(workflow)

# run the workflow
rapidfire(launchpad)
