from fireworks.core.firework import FiretaskBase, FWAction, Firework
from fireworks.utilities.fw_utilities import explicit_serialize
from fireworks.utilities.fw_serializers import load_object

@explicit_serialize
class DataLoaderTask(FiretaskBase):
    """ Sample solution for a data loader """

    _fw_name = 'DataLoaderTask'
    required_params = ['filename', 'outputs']
    optional_params = []

    def run_task(self, fw_spec):
        import json
        with open(self['filename'], 'r') as inp:
            data = json.load(inp)

        return FWAction(update_spec={self['outputs']: data})


@explicit_serialize
class RepeatIfLengthLesser(FiretaskBase):
    """ Sample conditional repeater """

    _fw_name = 'RepeatIfLengthLesser'
    required_params = ['measure', 'minimum']
    optional_params = []

    def run_task(self, fw_spec):
        if len(fw_spec[self['measure']]) < fw_spec[self['minimum']]:
            firework = Firework(
                tasks=[load_object(task) for task in fw_spec['_tasks']],
                spec=fw_spec,
                name='repeat '+self['measure']
            )
            return FWAction(detours=firework)
