#!/usr/bin/env python3
"""converter from fireworks to graphviz dot for visualization purposes"""
import argparse
from fireworks.fw_config import LAUNCHPAD_LOC
from fireworks.fw_config import CONFIG_FILE_DIR
from fireworks.scripts.lpad_run import get_lp
from fireworks.utilities.dagflow import DAGFlow


def id_type(value):
    """check the type of fw_id"""
    rvalue = int(value)
    if rvalue < 0:
        msg = f'{value} must have non-negative int value'
        raise argparse.ArgumentTypeError(msg)
    return rvalue


def parse_clargs():
    """parse the command line arguments for the script"""
    m_description = 'Converts a FireWorks workflow to graphviz DOT format'
    parser = argparse.ArgumentParser(description=m_description)
    parser.add_argument('-c', '--config_dir',
                        help='path to configuration file (if -l unspecified)',
                        default=CONFIG_FILE_DIR)
    parser.add_argument('-l', '--launchpad_file',
                        help='path to launchpad file', default=LAUNCHPAD_LOC)
    parser.add_argument('-f', '--filename', required=False,
                        help='filename for output', default='wf.dot')
    parser.add_argument('-v', '--view', default='combined', required=False,
                        help='View: one of controlflow, dataflow, combined')
    parser.add_argument('-i', '--fw_id', default=False, required=True,
                        help='firework id', type=id_type)
    return parser.parse_args()


def main():
    """main function"""
    clargs = parse_clargs()
    dag = DAGFlow.from_fireworks(get_lp(clargs).get_wf_by_fw_id(clargs.fw_id))
    dag.to_dot(filename=clargs.filename, view=clargs.view)


if __name__ == '__main__':
    main()
