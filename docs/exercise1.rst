Exercise 1: Managing control flow
=================================

With this exercise we will learn to describe the dependencies in control flow 
between Fireworks, exploit possible concurrencies and use the built-in Firetask 
``ScriptTask``. The ``ScriptTask`` is fully documented here:
https://materialsproject.github.io/fireworks/scripttask.html.

Problem 1.1
-----------

Change to directory **exercises/work/1_control_flow**.

Copy the workflow files from the folder **exercises/problems/1_control_flow**. 
For example, if you decide to use YAML::
    
  cp ../../problems/1_control_flow/*.yaml .

Then, for each sequential workflow (**f1_pitstop_seq_wrong_1.yaml**,
**f1_pitstop_seq_wrong_2.yaml**, and **f1_pitstop_seq_wrong_3.yaml**) carry out
the following steps:

* Inspect the workflow for errors and correct the errors
* Add the workflow to the LaunchPad checking again for errors
* Create a workflow graph and make sure it is correct; delete the workflow if
  necessary, correct it in the workflow file and add it again to launchpad
* Query the workflow state
* Execute the workflow in *singleshot* mode, i.e. run one Firework at a time.
  After running every Firework, monitor the output and the states of the Fireworks
  until the workflow is completed.


Problem 1.2
-----------

Repeat the steps of **Problem 1.1** for the parallel version of the workflow:
**f1_pitstop_par_wrong_1.json**, **f1_pitstop_par_wrong_2.json** and 
**f1_pitstop_par_wrong_3.json**. Try different modes to launch the workflows:

* ``rlaunch singleshot``: launch one firework from launchpad
* ``rlaunch rapidfire``: launch many fireworks in a sequence
* ``rlaunch multi <N>``: launch N fireworks in parallel, choose N from [2, 3, 4]

Add the flag *--help* to each of the launch modes to see all available options.
For the multi mode, try to find N and the parameters nlaunches, sleep and timeout
for which the best speedup can be achieved using the command::
  time rlaunch multi <N> --nlaunches <L> --sleep <sec> --timeout <sec>