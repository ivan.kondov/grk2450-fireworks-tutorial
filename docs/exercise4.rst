Exercise 4: Extending a workflow
================================

Workflows in FireWorks can be extended at any time of their life cycle, 
particularly in states *READY*, *RUNNING* and *COMPLETED*. If the appended 
Firework or workflow is appended as a detour then the child Fireworks may not be 
be *RUNNING* or *COMPLETED*.


Problem 4.1
-----------

Using the extend the workflow from the solution of **Problem 3.1** 
(**image_reconstruct.json**) with the workflow from **Problem 3.2** 
(**image_swirl.json**) to reuse the image produced by the former as input in the 
latter workflow. For this, first copy the workflow **image_swirl.json** from 
**exercises/solutions/3_files_and_commands** to a file 
**image_swirl_montaged.json**. Then do the following changes:

- Change all Firework IDs to become negative integers. This is required by the 
  ``append_wflow`` method.

- Add the following Firework to the list of Fireworks (for JSON adapt the code
  correspondingly)::

    - fw_id: -6
      name: Pass filename
      spec:
        _tasks:
        - _fw_name: PyTask
          func: auxiliary.print_func
          inputs: [montaged image]
          outputs: [montaged image]
  and::

     '-6': [-1, -2, -3, -4, -5]
  to the ``links`` dictionary. Why is this step necessary?

- Modify it so that the input ``original image`` in all Fireworks is passed from 
  a parent Firework::

    original image: {source: montaged image}

- Save the thus prepared sub-workflow file as **image_swirl_montaged.[yaml|json]**.

Now identify the Firework ID of the Firework providing the final image as output::

    lpad get_fws -n "Put the four pieces together"

Alternatively, the workflow can be found by name and the last Firework can be 
found in the more detailed printout::

    lpad get_wflows -n "Image reconstruction" -d more

Finally, use the command::

    lpad append_wflow -i <ID> -f image_swirl_montaged.json

to extend the workflow with the sub-workflow. A new query shows that the workflow 
is now marked from *COMPLETED* to *RUNNING*::

    lpad get_wflows -s "RUNNING"

Run the extended workflow.
